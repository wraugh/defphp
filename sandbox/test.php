<?php

require __DIR__ . '/../src/Defphp.php';

try {
    Defphp\Defphp::ex("Dang", "Well dang it works", 2001);
} catch (Dang $e) {
    error_log($e->getCode());
    error_log($e->getMessage());
} catch (Exception $e) {
    error_log("Nope, default handler");
}

Defphp\Defphp::alias();

try {
    ex("Welp,Yep", "Who wins? Depends on catcher", 2002);
} catch (Welp $e) {
    error_log("Welp " . $e->getCode());
    error_log($e->getMessage());
} catch (Yep $e) {
    error_log("Yep ". $e->getCode());
    error_log($e->getMessage());
} catch (Exception $e) {
    error_log("Nope, default handler");
}

try {
    ex("Welp,Yep", "Who wins? Depends on catcher", 2003);
} catch (Yep $e) {
    error_log("Yep ". $e->getCode());
    error_log($e->getMessage());
} catch (Welp $e) {
    error_log("Welp " . $e->getCode());
    error_log($e->getMessage());
} catch (Exception $e) {
    error_log("Nope, default handler");
}

try {
    ex("asdf-dsaf", "broken type", 2004);
} catch (Exception $e) {
    error_log("lol whoops " . $e->getCode());
    error_log($e->getMessage());
}

try {
    Defphp\Defphp::alias("Welp,Nope", "broken alias", 2005);
} catch (Exception $e) {
    error_log("k now " . $e->getCode());
    error_log($e->getMessage());
}


try {
    throw new \Exception("This is what a normal stack trace looks like");
} catch (\Exception $e) {
    error_log($e);
}

try {
    Defphp\Defphp::ex("TraceMe", "This adds one line to the stack trace");
} catch (TraceMe $e) {
    error_log($e);
}

try {
    ex("TraceMe", "This adds two lines to the stack trace");
} catch (TraceMe $e) {
    error_log($e);
}

try {
    try {
        throw new \Exception("throw is obsolete now", 2006);
    } catch (\Exception $e) {
        error_log("original: $e");
        ex("CloneEx", $e->getMessage(), $e->getCode(), $e);
    }
} catch (CloneEx $e) {
    error_log("clone: $e");
}

class CanEx {
    use \Defphp\Ex;
}
try {
    CanEx::ex("ClassEx", "this adds one line to the stack trace");
} catch (ClassEx $e) {
    error_log("from a class: $e");
}
