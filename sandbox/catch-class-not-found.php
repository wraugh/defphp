<?php

class MyException extends Exception {};

try {
    throw new MyException("It doesn't matter if inexistant classes are used as guards in catch blocks");
} catch (NotMyExceptionWhoops $e) {
    error_log("This never happens; we're matching a class that doesn't exist");
}
