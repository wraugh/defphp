<?php

register_shutdown_function(function () {
    $err = error_get_last();
    error_log("A fatal error has been caught; details: " . json_encode($err, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
});

class MyException extends Exception {};

try {
    throw new MyException();
} catch (MyExceptiafiduydCatOnKeyboard $e) {
    error_log("Guard doesn't match, but that's not fatal");
} catch (Exception $e) {
    error_log("We handle MyException in the catch-all exception handler");
}

try {
    throw new MyExceptiafiduydCatOnKeyboard();
} catch (MyExceptiafiduydCatOnKeyboard $e) {
    error_log("Guard _would_ match, but a fatal error has already been caused, and this code never runs");
} catch (Exception $e) {
    error_log("A fatal error is not an exception; this code never runs");
}

error_log("A fatal error has been caused; this code will never run");
