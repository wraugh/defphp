<?php

function err ($className) {
    eval("class $className extends Exception {};");
    throw new $className();
}

try {
    err("MyException");
} catch (MyExceptiafiduydCatOnKeyboard $e) {
    error_log("Guard doesn't match, but that's not fatal");
} catch (Exception $e) {
    error_log("We handle MyException in the catch-all exception handler");
}

try {
    err("MyExceptiafiduydCatOnKeyboard");
} catch (MyException $e) {
    error_log("A typo-on-throw prevents this guard from matching");
} catch (Exception $e) {
    error_log("We handle the misspelled exception in the catch-all handler");
}

error_log("No fatal error has been caused; this code runs normally");

