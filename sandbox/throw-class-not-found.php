<?php

try {
    if (false) {
        throw new DoesntMatter("This is not a real exception class, but this code is never run, so it causes no harm");
    } else {
        throw new Whoops("This is not a real exception class; this exception never bubbles up; instead, there's a 'class not found' error");
    }
} catch (Exception $e) {
    error_log("This never happens; an Error is not an Exception");
}
