<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

namespace Defphp;

trait Ex {
    public static function ex ($types, $message = "", $code = 0, $previous = null) {
        $interfaces = static::parseTypes($types);
        $className = static::defineClass($interfaces);

        throw new $className($message, $code, $previous);
    }


    private static function parseTypes ($types): array {
        if (!is_array($types)) {
            $types = explode(',', $types);
        }
        foreach ($types as $type) {
            if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $type)) {
                static::ex("ExTypes,Syntax", "Bad type name '$type'", 58241002);
            }
        }

        return $types;
    }

    private static function defineClass (array $interfaces): string {
        static::defineInterfaces($interfaces);
        $className = static::generateClassName();
        eval("class $className extends \Exception implements " . implode(', ', $interfaces) . " {};");

        return $className;
    }

    private static function defineInterfaces (array $interfaces) {
        foreach ($interfaces as $interface) {
            if (!interface_exists($interface, false)) {
                eval("interface $interface {};");
            }
        }
    }

    private static function generateClassName (): string {
        return str_replace('.', '', uniqid("Defphp", true));
    }
}
