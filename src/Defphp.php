<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

namespace Defphp;

require_once __DIR__ . '/Ex.php';

class Defphp {
    use Ex;

    public static function alias (string $alias = 'ex') {
        if (!preg_match(self::IDENT_REGEX, $alias)) {
            static::ex("ExAlias,Syntax", "Bad alias name '$alias'", self::ERR_ALIAS_SYNTAX);
        }
        eval("function $alias(\$types, \$message, \$errorCode = 0, \$previous = null) {" .
            "     \Defphp\Defphp::ex(\$types, \$message, \$errorCode, \$previous);" .
            " }"
        );
    }


    // Regex to validate function, class, or interface identifiers
    // Copied from http://php.net/manual/en/functions.user-defined.php
    // and from http://php.net/manual/en/language.oop5.basic.php
    const IDENT_REGEX = '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/';

    const ERR_ALIAS_SYNTAX = 58241001;
}
