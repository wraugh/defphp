2017-05-19
==========

I'm not sure this is a good idea or whether it can be built, but here's some
notes to start from.

1. Defining exception classes is a waste of time. All exceptions are the same,
   the only reason to define them is so they can be matched in catch blocks.

2. There's a difference between an exception class and an error code. Error
   codes should be unique to the place where the exception is thrown. They
   shouldn't be used as guards in catch blocks; that's what the exception's
   class hierarchy is for.

I was using error codes because I was too lazy to define exception classes, e.g.

    try {
        $thing = $this->things->get($url);
    } catch (\Exception $e) {
        if ($e->getCode() !== Things::ERR_NOT_FOUND) {
            throw $e;
        }
        $thing = $this->things->create($url);
    }

This would more naturally be written as

    try {
        $thing = $this->things->getByUrl($url);
    } catch (ThingNotFoundException $e) {
        $thing = $this->things->create($url);
    }

but that would require me to write

    class ThingNotFoundException extends Exception {};

somewhere in the codebase. And that's a meaningless line to write. So I was
willing to live with the extra verbosity of using error codes to match
exceptions to handlers. Until I found myself writing this:

    try {
        $thing = $this->db->fetchOne($query, [$url]);
    } catch (\Exception $e) {
        if ($e->getCode() !== Db::ERR_NO_ROWS) {
            throw $e;
        }
        throw new \Exception("Thing with url '$url' not found", self::ERR_NOT_FOUND);
    }

Here I was, catching an error in the database layer, only to re-throw it with a
new error code. Not catching it would have been unthinkable: having the caller
need to know the inner details just to be able to catch the correct exception is
not something I'm willing to put up with. But having to re-write the exception
code at every layer in the stack (and losing the original exception details in
the process) is not a great alternative. Still, I preferred it to the tedious
bookkeeping that I would need to do to use php's native catch functionality. But
I soon found myself writing this:

    try {
        $thing = $this->db->fetchOne($query, [$id]);
    } catch (\Exception $e) {
        if ($e->getCode() !== Db::ERR_NO_ROWS) {
            throw $e;
        }
        throw new \Exception("Thing #$id not found", self::ERR_NOT_FOUND);
    }

That is, I was re-using the `ERR_NOT_FOUND` error code, because I didn't want to
have to split it into `ERR_NOT_FOUND_BY_URL` and `ERR_NOT_FOUND_BY_ID`, which is
ridiculous. This problem wouldn't exist had the following classes been
available:

    interface NotFound {};

    class DbNoRowsException extends Exception implements NotFound {};

    class ThingNotFoundException extends Exception implements NotFound {};
    class ThingNotFoundByUrlException extends ThingNotFoundException;
    class ThingNotFoundByIdException  extends ThingNotFoundException;

This would allow me to catch using PHP's built-in matching, at any level of
detail the situation needs. But dang, that's a lot of book-keeping.

Is it possible to automate the book-keeping by defining exception classes on the
fly when they are thrown? With creative use of evals and existence checks it
should be possible. The challenge is to get the UI right such that we don't
introduce too many new failure modes to programmers.

Probably the simplest way is to forgo inheritance and just use interfaces (the
same effect can be achieved). I can ensure that all the requested interfaces
exist, then define a class with a unique name that implements all those
interfaces, then throw it. I would catch by matching interfaces.

For the purposes of exception matching, the above example can be rewritten like so:

    interface NotFound {};
    interface DbNoRows {};
    interface ThingNotFound {};
    interface ThingNotFoundByUrl {};
    interface ThingNotFoundById {};

    class E1 extends Exception implements DbNoRows, NotFound {};
    class E2 extends Exception implements ThingNotFound, NotFound {};
    class E3 extends Exception implements ThingNotFoundByUrl, ThingNotFound, NotFound {};
    class E4 extends Exception implements ThingNotFoundById, ThingNotFound, NotFound {};


2017-05-23
==========

If I had a function that would define the classes on-the-fly, I could call it
like this:

    public function fetchOne ($query, array $params = null) {
        $rows = $this->fetchAll($query, $params);
        if (count($rows) != 1) {
            $msg = "Expected exactly one row but got " . count($rows) . " from query $query";
            if (isset($params)) {
                $msg .= " w/ params " . json_encode($params);
            }
            $errorClasses = ['Db', 'DbUnexpectedRows'];
            if (count($rows) > 1) {
                $errorClasses[] = 'DbTooManyRows';
                $errorCode = self::ERR_TOO_MANY_ROWS;
            } else {
                $errorClasses[] = 'DbNoRows';
                $errorClasses[] = 'NotFound';
                $errorCode = self::ERR_NO_ROWS;
            }
            myTheoreticalExceptionMacro($errorClasses, $errorCode);
        }

        return reset($rows);
    }

Failure Modes
-------------

In stock PHP, there are two ways to screw up when working with exceptions:

    1. trying to catch a class that doesn't exist, or
    2. trying to throw a class that doesn't exist.

When trying to catch a class that doesn't exist, the catch block never matches,
and the exception bubbles up.

Trying to throw a class that doesn't exist causes a PHP fatal error. A fatal
error is not an exception. It can be "caught" by registering a global error
handler, but that's a different mechanism that has nothing to do with
exceptions.

For example,

    <?php

    class MyException extends Exception {};

    try {
        throw new MyException();
    } catch (MyExceptiafiduydCatOnKeyboard $e) {
        error_log("Guard doesn't match, but that's not fatal");
    } catch (Exception $e) {
        error_log("We handle MyException in the catch-all exception handler");
    }

    try {
        throw new MyExceptiafiduydCatOnKeyboard();
    } catch (MyExceptiafiduydCatOnKeyboard $e) {
        error_log("Guard _would_ match, but a fatal error has already been caused, and this code never runs");
    } catch (Exception $e) {
        error_log("A fatal error is not an exception; this code never runs");
    }

    error_log("A fatal error has been caused; this code will never run");

Were I to create a function that defines exception classes on-the-fly as needed,
the failure modes would remain the same (i.e. making a typo in exception names).
However, it would no longer be possible to trigger class-not-found fatal errors.
Rather, when misspelling an exception on throw, it would get created anyway.
This means it could be caught by default exception handlers. This is pretty much
equivalent if all catch block guards have high specificity: the misspelled
exception would bubble uncaught all the way to the top, where it would trigger
an uncaught-exception fatal error. But this isn't equivalent when there are
catch-all exception blocks; then the misspelled exception would get handled
there. This will make it trickier to catch these typos.

For comparison,

    <?php

    function err ($className) {
        eval("class $className extends Exception {};");
        throw new $className();
    }

    try {
        err("MyException");
    } catch (MyExceptiafiduydCatOnKeyboard $e) {
        error_log("Guard doesn't match, but that's not fatal");
    } catch (Exception $e) {
        error_log("We handle MyException in the catch-all exception handler");
    }

    try {
        err("MyExceptiafiduydCatOnKeyboard");
    } catch (MyException $e) {
        error_log("A typo-on-throw prevents this guard from matching");
    } catch (Exception $e) {
        error_log("We handle the misspelled exception in the catch-all exception handler");
    }

    error_log("No fatal error has been caused; this code runs normally");

I'm hoping that having it be easy to throw well-typed exceptions will make all
catch block guards specific, such that the default Exception catch block will be
reserved for uncaught (i.e. fatal) exceptions. This would make the with-macro
code fail the same way as the stock php code.

I think the possibility of writing harder-to-find typos if you're bad at writing
catch blocks is an acceptable price to pay for the convenience of throwing
well-typed exceptions without having to define them beforehand.
